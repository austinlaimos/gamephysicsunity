﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionManager : MonoBehaviour {

    BoundingParent[] colliders;
    //List<Particle2D> objects;

    public static CollisionManager instance;

    void Start(){
        colliders = GameObject.FindObjectsOfType<BoundingParent>();
        instance = this;
    }

    void Update(){

    }

    public bool CheckCollisions(BoundingParent obj1){
        for(int i = 0; i < colliders.Length; i++){
            if(colliders[i] != obj1 && colliders[i].CheckCollision(obj1)){
                return true;
            }
        }
        return false;
    }
}
