﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoundingBox : BoundingParent {

    #pragma warning disable CS0649
    [SerializeField] Vector3 cubeSize;
    #pragma warning restore CS0649

    [HideInInspector] public Vector3 realHalf;

    void Start(){
        type = BoundingType.Cube;
        realHalf = new Vector3(cubeSize.x * transform.localScale.x, cubeSize.y * transform.localScale.y, cubeSize.z * transform.localScale.z);
        realHalf = realHalf * .5f;
    }

    void Update(){

    }

    void OnDrawGizmos(){
        Gizmos.color = Color.red;
        Gizmos.matrix = transform.localToWorldMatrix;
        Gizmos.DrawWireCube(center, cubeSize);
    }

    public override bool CheckCollision(BoundingParent other){
        if(other.type == BoundingType.Cube){
            BoundingBox otherCube = other as BoundingBox;
            //Adapted from my framework: Camel3D

            Vector3 min = center + transform.position - realHalf;
            Vector3 max = center + transform.position + realHalf;

            Vector3 otherMin = otherCube.center + otherCube.transform.position - otherCube.realHalf;
            Vector3 otherMax = otherCube.center + otherCube.transform.position + otherCube.realHalf;

            bool xCol = min.x <= otherMax.x && max.x >= otherMin.x;
            bool yCol = min.y <= otherMax.y && max.y >= otherMin.y;
            bool zCol = min.z <= otherMax.z && max.z >= otherMin.z;

            return xCol && yCol && zCol;
        }
        return false;
    }
}
