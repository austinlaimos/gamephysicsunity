﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Particle2D: MonoBehaviour {

    public enum IntegrationType { EulerExplicit, Kinematic };

    #pragma warning disable CS0649
    //Step 1
    [SerializeField] Vector2 pos;
    [SerializeField] Vector2 vel;
    [SerializeField] Vector2 accel;
    [SerializeField] float rot;
    [SerializeField] float angVel;
    [SerializeField] float angAccel;
    [SerializeField] bool useTransformForPosition;
    [SerializeField] bool useGravity;
    [SerializeField] bool useTest;
    [SerializeField] float mass;

    [SerializeField] IntegrationType positionType;
    [SerializeField] IntegrationType rotationType;
    #pragma warning restore CS0649

    public bool isSleeping;

    BoundingParent bounding;

    void Start(){
        bounding = GetComponent<BoundingParent>();
        if(useTransformForPosition){
            pos = transform.position;
            rot = transform.rotation.z;
        }
        if(useGravity){
            accel.y = -9.8f;
        }
    }

    void FixedUpdate(){
        //Step 3: Apply integration technique
        if(positionType == IntegrationType.EulerExplicit){
            UpdatePositionEulerExplicit(Time.fixedDeltaTime);
            
        }
        else if(positionType == IntegrationType.Kinematic){
            UpdatePositionKinematic(Time.fixedDeltaTime);
        }

        if(rotationType == IntegrationType.EulerExplicit){
            UpdateRotationEulerExplicit(Time.fixedDeltaTime);

        }
        else if(rotationType == IntegrationType.Kinematic){
            UpdateRotationKinematic(Time.fixedDeltaTime);
        }
        if(bounding && (pos != Vector2.zero || rot != 0)){
            if(!CollisionManager.instance.CheckCollisions(bounding)){
                transform.position = pos;
                rot %= 3.14f;
                transform.rotation = new Quaternion(transform.rotation.x, transform.rotation.y, rot, transform.rotation.w);

                //Step 4:
                //Test Case
                //(Set the initial values)
                if (useTest){
                    accel.y = -Mathf.Sin(Time.fixedTime);
                    //angAccel = -Mathf.Sin(Time.fixedTime);
                    //Bells and Whistles:
                    if(useGravity){
                        accel += new Vector2(Physics.gravity.x, Physics.gravity.y);
                    }
                }
            }
        }
		else {
			pos = transform.position;
            rot = transform.rotation.z;
			
			vel = new Vector2(0f, 0f);
			
			//TODO: Fix acceleration downward
		}
    }
    
    //Step 2:
    //Make dt a non-zero
    void UpdatePositionEulerExplicit(float dt){
        //x(t+dt) = x(t) + v(t)dt
        //F(t+dt) = F(t) + f(t)dt
        //               + (dF/dt)dt

        pos += vel * dt;
        vel += accel * dt;
    }

    void UpdatePositionKinematic(float dt){
        //x(t+dt) = x(t) + v(t)dt + .5 * a(t)dt * dt
        pos += vel * dt + .5f * accel * dt * dt;
        vel += accel * dt;
    }

    void UpdateRotationEulerExplicit(float dt){
        rot += angVel * dt;
        angVel += angAccel * dt;
    }

    void UpdateRotationKinematic(float dt){
        rot += angVel * dt + .5f * angAccel * dt * dt;
        angVel += angAccel * dt;
    }
}
