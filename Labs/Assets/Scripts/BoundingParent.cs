﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BoundingParent : MonoBehaviour {

    public enum BoundingType { Cube, Sphere };
    [HideInInspector] public BoundingType type;
    public Vector3 center;

    void Start(){
        
    }

    void Update(){
        
    }

    public abstract bool CheckCollision(BoundingParent other);
}
